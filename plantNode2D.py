from enum import Enum
from vector import VectorObject2D


class NodeType(Enum):

    """  Flag for each node used in determining branch probabilities"""
    ROOT = 0

    UNBRANCHED = 1
    UNBRANCHED_MONO = 5
    """the node is unbranched: starting : mono behaves differently """
    MAIN = 10
    """The node has grown the main axis"""
    LEFT_BRANCHED = 20
    """the node has branched left"""
    RIGHT_BRANCHED = 30
    """the node has branched right""" 
    LR_BRANCHED = 60
    """ the node has branched both left and right"""
    MAIN_LEFT = 40
    """ The main axis has grown left ( alternate )"""
    MAIN_RIGHT = 50
    """The main axis has grown right ( alternate )"""
    MAIN_L = 45
    """ The main axis has grown left ( monopodial )"""
    MAIN_R = 55
    """The main axis has grown right ( monopodial )"""
    MAIN_LR = 65
    """ the node has branched both left and right"""
    NO_MAIN_L = 42
    """ The main axis has not grown but branched right ( monopodial)"""
    NO_MAIN_R = 52
    """ The main axis has not grown right but branched left ( monopodial)"""
    NO_MAIN_LR = 62
    """ The main axis has not grown right but branched left and right( monopodial)"""
    FLOWERED = 70
    """the node has flowered and terminated"""
    DEAD = 80
    """The node has died"""


# lists holding  the identifiers for  tree species, individual  trees and individual nodes
# altered current_tree_id to -1. not sure why.
current_tree_id = 0
current_tree_node_id = []


class PlantNode2D:
    """ Node object that corresponds to a plant section.
      It contains a tree_id and the node_id for which the tuple should be unique
    """
    tree_species_id: int = None
    """Species identifier  for the plant determining growth characteristics """
    tree_id: int = None
    """Individual plant identifier"""
    node_id: int = None
    """Individual node identifer"""
    position: VectorObject2D = None
    """Starting position for plant  ( effectively seed distribution )"""
    direction: VectorObject2D = None
    """Starting direction for growth growth direction and branch angle   """
    node_type: NodeType = None
    """Determines branching pattern"""
    int_node_distance: float = None
    """Internode distance passed to increment plant size"""

    # ADD SOME EXTRA INFO THAT YOU CAN USE AS THE HISTORY OF THE TREE GROWS

    def __init__(self,
                position: VectorObject2D,
                tree_sp_id: int,
                int_node_distance: float,
                direction: VectorObject2D = None, 
                tree_id: int = None,          
                node_type: NodeType = NodeType.UNBRANCHED,             
                ):
   
        """Generate an new plant node instance

        Args:
            position (float,float): X,Y  position of the node
            tree_sp_id (int): identifies the species parameters defining growth
            tree_id (int, optional): Tree ID, use none if you want to create a new tree instance. Defaults to None.
            node_type (NodeType, optional): Node type. Did default to NodeType.TRUNK.
            direction (Vector2D): Direction the plant node is growing in. Defaults to None.
            internode_distance (float): the indernode distance used to calculate position
                and size of plant.
         """
        self.position = position
        self.direction = direction
        self.node_type = node_type
        self.tree_species_id = tree_sp_id
        self.tree_id = tree_id
        self.int_node_distance = int_node_distance
        # If there is no direction stated head directly up
        if direction is None:
            self.direction = VectorObject2D(x=1, y=0)
        else:
            # Here we normalize to make sure we don't do stupid things
            self.direction = direction / direction.rho

        # We are creating a new tree, let's create it and add necessary values
        if tree_id == current_tree_id + 1 or tree_id == None:
            self.tree_id = current_tree_id  
            # Here we need to add a new entry because we have a new tree
            current_tree_node_id.append(1)
            self.node_id = 0
        # Here we are just adding a node to the tree
        else:
            # Let's make sure the tree id is done in order
            if tree_id > current_tree_id+1:
                Exception("Tree ID is higher than the current tree ID")
                # added a -1 to the tree_id tp keep it in range
            self.node_id = current_tree_node_id[tree_id-1]
            current_tree_node_id[tree_id-1] += 1

    def __lt__(self, other):
        """This is the implementation of the < parameter for the class plantNode
        This is used to get the sorted graph

        Args:
            other (plantNode): Other plant node to give

        Returns:
            bool: returns the less than first for the tree ID then with the nodeID.
        """
        if self.tree_id == other.tree_id:
            return self.node_id < other.node_id
        return self.tree_id < other.tree_id
