from enum import Enum
from vector import VectorObject3D


class NodeType(Enum):
    TRUNK = 1
    BRANCH = 2
    LEAF = 3
    LEFT = 4
    RIGHT = 5
    BOTH = 6
    NONE = 7
    APEX = 8
    FLOWER = 9

current_tree_id = -1
current_tree_node_id = []
class PlantNode:
    """ Node object that corresponds to a tree section.
    
    It contains a tree_id and the node_id for which the tuple should be unique
    """
    tree_id:int = None
    node_id:int = None
    position:VectorObject3D = None
    growth_direction:VectorObject3D = None
    node_type: NodeType = NodeType.TRUNK

    def __init__(self,
                 position:VectorObject3D,
                 tree_id:int = None,
                 node_type:NodeType = NodeType.TRUNK,
                 growth_direction:VectorObject3D = None):
        """Generate an new plant node instance

        Args:
            position (float,float,float): X,Y,Z position of the node
            tree_id (int, optional): Tree ID, use none if you want to create a new tree instance. Defaults to None.
            node_type (NodeType, optional): Node type. Defaults to NodeType.TRUNK.
            node_direction (Vector3D, optional): Direction the plant node is growing in. Defaults to None.
        """
        self.position = position
        self.node_type = node_type

        # If there is no direction stated head directly up
        if growth_direction is None:
            self.growth_direction = VectorObject3D(x=1,y=0,z=0)
        else:
            # Here we normalize to make sure we don't do stupid things
            self.growth_direction = growth_direction / growth_direction.mag


        # We are creating a new tree, let's create it and add necessary values
        if tree_id == current_tree_id + 1 or tree_id == None:
            self.tree_id = current_tree_id
            current_tree_id
            # Here we need to add a new entry because we have a new tree
            current_tree_node_id.append(1)
            self.node_id = 0
        # Here we are just adding a node to the tree
        else:
             # Let's make sure the tree id is done in order
            if tree_id > current_tree_id+1:
                Exception("Tree ID is higher than the current tree ID")
            self.tree_id = tree_id
            self.node_id = current_tree_node_id[tree_id]
            current_tree_node_id[tree_id] += 1

    def __lt__(self, other):
        """This is the implementation of the < parameter for the class plantNode
        This is used to get the sorted graph

        Args:
            other (plantNode): Other plant node to give

        Returns:
            bool: returns the less than first for the tree ID then with the nodeID.
        """
        if self.tree_id == other.tree_id:
            return self.node_id < other.node_id
        return self.tree_id < other.tree_id