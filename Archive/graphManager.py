from plantNode import PlantNode
from plantNode import NodeType
import networkx as nx
from vector import VectorObject3D
from vector import VectorObject2D
import matplotlib.pyplot as plt
import numpy as np
import settings
from settings import GENERATIONS
from settings import SPECIES1
from util import get_new_growth_direction
from util import create_nodes_in_3Ddirection
from util import create_node_in_2Ddirection


class GraphManager:
    

    graph:nx.DiGraph = None

    def __init__(self) -> None:
        # You are likely to use directed graphs and also growing network
        # https://en.wikipedia.org/wiki/Evolving_network 
        # Perhaps one of the function might accelerate graph creation
        # gn_graph, gnc_graph, gnr_graph
        self.graph = nx.DiGraph()

  
    def createPlant(self, position:VectorObject2D):
        """Creates a new tree in the desired position

        Args:
            position (VectorObject3D): Position of the initial node of the tree. Needs to have z=0
        """
        #if position.z !=0:
         #   Exception("Tree is not connected to the ground")

        # Lets create the nodes
        #trunk_nodes_amount = settings.TREE_AVERAGE_NODES + np.random.randint(-settings.TRUNK_NODES_DEVIATION, settings.TRUNK_NODES_DEVIATION)
        trunk_nodes_amount = 5;
        for i in range (GENERATIONS):
            trunk_nodes = create_node_in_2Ddirection(position,
                                                        VectorObject2D(x=0, y=0),
                                                        SPECIES1.BRANCH_ANGLE,
                                                        SPECIES1.INTERNODE,
                                                        SPECIES1.INTN_SD,
                                                        trunk_nodes_amount,
                                                        )
             # Now lets add the nodes to the graph
            self.graph.add_nodes_from(trunk_nodes)  
            for i in range(len(trunk_nodes)-1):
                self.graph.add_edge(trunk_nodes[i],trunk_nodes[i+1])
             # Now let's add the branch
             # Let's get a vector of randoms to see where branch start
            branch_random = np.random.uniform(size=len(trunk_nodes))
            branches = branch_random < settings.MBRANCH_PROB1

             # now let's add branches
            for i in range(len(branches)):
                if not branches[i]:
                  continue
            
                branch_direction = get_new_growth_direction(trunk_nodes[i].growth_direction,
                                                    settings.BRANCH_ANGLE1-settings.BRANCH_SD1,
                                                    settings.BRANCH_ANGLE1+settings.BRANCH_SD1,
                                                    )
                
                #number_of_branch_nodes = np.random.random_integers(
                   #  settings.BRANCH_AVERAGE_NODES-settings.BRANCH_NODE_DEVIATION,
                   # settings.BRANCH_AVERAGE_NODES+settings.BRANCH_NODE_DEVIATION
                   # )
                branch_node = create_node_in_2Ddirection(
                    trunk_nodes[i].position,
                    branch_direction,
                    #call the relevant settings for the first plant
                    settings.BRANCH_ANGLE,
                    settings.BRANCH_SD,
                    settings.INTERNODE,
                    settings.INTN_SD,
                    #number_of_branch_nodes,
                    trunk_nodes[i].tree_id,
                    False,
                    )
                 # Now lets add the trunk nodes to the graph
                self.graph.add_edge(trunk_nodes[i],branch_node[0])
                self.graph.add_nodes_from(branch_node)
                for i in range(len(branch_node)-1):
                    self.graph.add_edge(branch_node[i],branch_node[i+1])
  


    def createTree(self, position:VectorObject3D):
        """Creates a new tree in the desired position

        Args:
            position (VectorObject3D): Position of the initial node of the tree. Needs to have z=0
        """
        if position.z !=0:
            Exception("Tree is not connected to the ground")

        # Lets create the trunk nodes
        trunk_nodes_amount = settings.TREE_AVERAGE_NODES + np.random.randint(-settings.TRUNK_NODES_DEVIATION, settings.TRUNK_NODES_DEVIATION)
        #trunk_nodes_amount = 5;
        for i in range (GENERATIONS):
            trunk_nodes = create_nodes_in_3Ddirection(position,
                                                        VectorObject3D(x=0, y=0, z=1),
                                                        settings.TREE_TRUNK_ANGLE_DEVIATION,
                                                        settings.TRUNK_INTERNODE_DISTANCE,
                                                        settings.TRUNK_INTERNODE_DEVIATION,
                                                        trunk_nodes_amount,
                                                        )
             # Now lets add the trunk nodes to the graph
            self.graph.add_nodes_from(trunk_nodes)  
            for i in range(len(trunk_nodes)-1):
                self.graph.add_edge(trunk_nodes[i],trunk_nodes[i+1])
             # Now let's add the branch
             # Let's get a vector of randoms to see where branch start
            branch_random = np.random.uniform(size=len(trunk_nodes))
            branches = branch_random < settings.BRANCH_PROBABILITY

             # now let's add branches
            for i in range(len(branches)):
                if not branches[i]:
                  continue
            
                branch_direction = get_new_growth_direction(trunk_nodes[i].growth_direction,
                                                    settings.BRANCH_START_ANGLE-settings.BRANCH_START_ANGLE_DEVIATION,
                                                    settings.BRANCH_START_ANGLE+settings.BRANCH_START_ANGLE_DEVIATION,
                                                    )
                
                number_of_branch_nodes = np.random.random_integers(
                     settings.BRANCH_AVERAGE_NODES-settings.BRANCH_NODE_DEVIATION,
                    settings.BRANCH_AVERAGE_NODES+settings.BRANCH_NODE_DEVIATION
                    )
                branch_node = create_nodes_in_3Ddirection(
                    trunk_nodes[i].position,
                    branch_direction,
                    settings.BRANCH_NODES_ANGLE_DEVIATION,
                    settings.BRANCH_INTERNODE_DISTANCE,
                    settings.BRANCH_INTERNODE_DEVIATION,
                    number_of_branch_nodes,
                    trunk_nodes[i].tree_id,
                    False,
                    )
                 # Now lets add the trunk nodes to the graph
                self.graph.add_edge(trunk_nodes[i],branch_node[0])
                self.graph.add_nodes_from(branch_node)
                for i in range(len(branch_node)-1):
                    self.graph.add_edge(branch_node[i],branch_node[i+1])
               
    def displayGraph(self):
        """This function will display all the trees together.
        """
        node_xyz = np.array([(v.position.x, v.position.y, v.position.z) for v in sorted(self.graph)])
        edge_xyz = np.array([((u.position.x, u.position.y, u.position.z) , (v.position.x, v.position.y, v.position.z) ) for u, v in self.graph.edges()])


        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")

        # Plot the nodes - alpha is scaled by "depth" automatically
        ax.scatter(*np.transpose(node_xyz), s=10, ec="w")

        # Plot the edges
        for vizedge in edge_xyz:
            ax.plot(*vizedge.T, color="tab:green")

        ax.axis('equal')
        plt.tight_layout()
        plt.show()