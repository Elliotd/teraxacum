from dataclasses import dataclass
from enum import Enum


class GrowthPattern(Enum):

    """Flag for the branching behaviour of plant growth"""

    SYMPODIAL = 1
    ALTERNATE = 2
    MONOPODIAL = 3


# Population Number of each species
SP_POP_SIZES = [10, 0, 0]
"""Numbers of individuals for  each species using the SP_IDENTIFIER specific  parameters """
# Number of species and plants
SP_NUM = len(SP_POP_SIZES)
PLANT_NUM = sum(SP_POP_SIZES)

GENERATIONS = 5 
""" Number of iterations of growth cycle"""

# Habitat size
HABITAT_SIZE = 1000
"""Defines the limits of the area within which the plants will be seeded"""
HABITAT_NO_GROW = False
# Are there no grow areas in the habitat?
NUM_NOGROW_AREAS = 50
"""Number of areas within the habitat where growth is restricted"""
#should be calculated from the % of the habitat size to be reduced
# and the calculated area of each no_grow_area
NO_GROW_SIZE = 10
NO_GROW_RANDOM = True
NO_GROW_LINEAR = False
NO_GROW_REGULAR = False

# Percent occupied by no grow areas
PLOT_HABITAT = True
"Boolean to plot the habitat"
NO_GROW_REDUCTION_PERCENT = 90
"""Percentage of growth reduction within the no_grow_areas"""





# Species Parameters

@dataclass
class Species1:
    """Note Probabilities are set high as conditions for growth are set < than probabilities"""
    NAME: str
    """Species Name"""
    COLOUR: str
    """Colour of the internodes for plotting"""
    GROWTH_PATTERN: GrowthPattern
    """ Enum BranchType  defining the functionality of growth_tick: sympodial, monopodial, alternate  branching"""
    TERMINAL_ANGLE: float
    """ Mean terminal angle"""
    TERMINAL_SD: float
    """ Apex angle deviation"""
    LATERAL_ANGLE: float
    """ Mean Lateral Branch Angle"""
    LATERAL_SD: float
    """ Lateral Branch angle deviation"""
    MAIN_INTERNODE: float
    """ Mean internode distance"""
    MAIN_INTN_SD: float
    """ Standard deviation for main internode distance"""
    LATERAL_INTERNODE: float
    """ Mean  lateral internode distance"""
    LATERAL_INTERNODE_SD: float
    """ Standard deviation for lateral internode distance"""
    TERMINAL_GROWTH_PROB: float
    """ Probability of apex growth (main branch) """
    LATERAL_GROWTH_PROB: float
    """ Probability of lateral branching if the apex HAS branched """
    LATERAL_IF_NO_MAIN_PROB: float
    """ Probability of lateral branching if the apex has NOT branched"""
    NODE_NUMBER_TO_EXTENTION: int
    """Number of nodes from apex before full extension reached."""
    INTRA_COMP_PROB: float
    """ Change (Reduction) in branch probabilities if 
        the interference distance is reached WITHIN species. """
    INTER_COMP_PROB: float
    """ Change (Reduction) in branch probabilities if 
        the interference distance is reached BETWEEN species """
    INTRA_COMP_INTERNODE: float
    """ Change (Reduction) in internode distance if 
        the interference distance is reached WITHIN species """
    INTER_COMP_INTERNODE: float
    """ Change (Reduction) in internode distance if 
        the interference distance is reached BETWEEN species """
    INTRA_DISTANCE: float
    """ Interference distance between two nodes WITHIN species"""
    INTER_DISTANCE: float
    """ Interference distance between two nodes BETWEEN species"""
    NODES_BEFORE_DEATH: int
    """ Number of nodes before death takes the oldest nodes"""
    NODES_BEFORE_FLOWERING: int
    """ Number of nodes before flowering initiated."""


LIST_OF_SPECIES1: list[Species1] = [   
    Species1(
        NAME = "Species 0",
        COLOUR = '#f51505',
        GROWTH_PATTERN = GrowthPattern.SYMPODIAL,
        TERMINAL_ANGLE = 1,
        TERMINAL_SD = 0.1,
        LATERAL_ANGLE = 1,    
        LATERAL_SD = 0.1,
        MAIN_INTERNODE = 50, 
        MAIN_INTN_SD = 0.5,
        LATERAL_INTERNODE = 50,
        LATERAL_INTERNODE_SD = 0.5,
        TERMINAL_GROWTH_PROB = 1,
        LATERAL_GROWTH_PROB = 0.7,
        LATERAL_IF_NO_MAIN_PROB = 1,
        NODE_NUMBER_TO_EXTENTION = 1,
        INTRA_COMP_PROB = 0.95,
        INTER_COMP_PROB = 0.99,
        INTRA_COMP_INTERNODE = 3,
        INTER_COMP_INTERNODE = 5,
        INTRA_DISTANCE = 25,
        INTER_DISTANCE = 40,
        NODES_BEFORE_DEATH = 50,
        NODES_BEFORE_FLOWERING = 30),

    Species1(
        NAME = "Species 1",
        COLOUR = '#0df505',
        GROWTH_PATTERN = GrowthPattern.ALTERNATE,
        TERMINAL_ANGLE = 0.3,
        TERMINAL_SD = 0.1,
        LATERAL_ANGLE = 0.7,    
        LATERAL_SD = 0.3,
        MAIN_INTERNODE = 50, 
        MAIN_INTN_SD = 0.5,
        LATERAL_INTERNODE = 20,
        LATERAL_INTERNODE_SD = 0.5,
        TERMINAL_GROWTH_PROB = 1,
        LATERAL_GROWTH_PROB = 0.5,
        LATERAL_IF_NO_MAIN_PROB = 0.7,
        NODE_NUMBER_TO_EXTENTION = 1,
        INTRA_COMP_PROB = 1,
        INTER_COMP_PROB = 1,
        INTRA_COMP_INTERNODE = 3,
        INTER_COMP_INTERNODE = 5,
        INTRA_DISTANCE = 10,
        INTER_DISTANCE = 30,
        NODES_BEFORE_DEATH = 50,
        NODES_BEFORE_FLOWERING = 30),

    Species1(
        NAME = "Species 2",
        COLOUR = '#0539f5',
        GROWTH_PATTERN = GrowthPattern.MONOPODIAL,
        TERMINAL_ANGLE = 0,
        TERMINAL_SD = 0.1,
        LATERAL_ANGLE = 1.0,    
        LATERAL_SD = 0.1,
        MAIN_INTERNODE = 50, 
        MAIN_INTN_SD = 0.5,
        LATERAL_INTERNODE = 10,
        LATERAL_INTERNODE_SD = 0.5,
        TERMINAL_GROWTH_PROB = 1,
        LATERAL_GROWTH_PROB = 0.6,
        LATERAL_IF_NO_MAIN_PROB = 0.8,
        NODE_NUMBER_TO_EXTENTION = 1,
        INTRA_COMP_PROB = 0.5,
        INTER_COMP_PROB = 0.8,
        INTRA_COMP_INTERNODE = 3,
        INTER_COMP_INTERNODE = 3,
        INTRA_DISTANCE = 5,
        INTER_DISTANCE = 4,
        NODES_BEFORE_DEATH = 50,
        NODES_BEFORE_FLOWERING = 30) 
]
