import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from vector import VectorObject2D
from collections import defaultdict
from enum import Enum
import math
from plantNode2D import PlantNode2D, NodeType 
from settings import *

import random


def get_new_growth_direction2D(initial_direction: VectorObject2D, left: bool, branch_angle: float, branch_angle_sd: float, node_type: NodeType) -> VectorObject2D:
    """Returns a new growth direction from an initial growth direction, changed branch flag to NodeType

    Args:
        initial_direction (vectorObject2D): direction of growth of current internode and node
        branch_angle (float): species specific branch angle in radians
        branch_angle_sd (float): standard deviation of branch angle in radians
        branch_flag (int): flag indicating previous branching history

    Returns:
        new_growth_direction (vectorObject2D) : New direction for growth
    """

    # case statement for actions dependent on  the previous node's branching

    new_growth_direction: VectorObject2D
    if left:
        new_growth_direction = initial_direction.rotateZ(
            -np.random.normal(branch_angle, branch_angle_sd))
    else:
        new_growth_direction = initial_direction.rotateZ(
            np.random.normal(branch_angle, branch_angle_sd))
    return new_growth_direction



def create_node_in_2Ddirection(initial_position: VectorObject2D,
                               initial_direction: VectorObject2D,
                               branch_angle: float,
                               branch_angle_sd: float,
                               left: bool,
                               internode_distance: float,
                               internode_distance_sd: float,
                               node_type: NodeType,
                               tree_species_id: int,
                               tree_id: int,
                               no_grow_areas: list[tuple[float, float, float]] = []
                              ) -> PlantNode2D:
    """Generates a single plant node with selected settings

    Args:
        initial_position (VectorObject3D): initial position for the nodes
        initial_direction (VectorObject3D): initial direction for the growth
        branch_angle (float): angle variation for the growth
        branch_angle_sd (float): angle variation for the growth
        internode_distance (float): internode distance for the nodes
        internode_distance_variation (float): internode distance variation for the nodes
        node_type (NodeType): type of node to be created
        tree_species_id (int): species id for the tree
        tree_id (int): tree id for the tree
        no_grow_areas (list[tuple[float, float, float]], optional): list of no grow areas. Defaults to [].
    Returns:
        list[PlantNode]: _description_
    """

    newNodeDistance = np.random.normal(
        internode_distance, internode_distance_sd)
    newNodeDirection = get_new_growth_direction2D(
        initial_direction, left, branch_angle, branch_angle_sd, node_type,)
    newNodePosition = initial_position + newNodeDistance * newNodeDirection
    return PlantNode2D(newNodePosition, tree_species_id, newNodeDistance, newNodeDirection, tree_id)

def displayGraph(population: nx.DiGraph):
    """This function will display all the trees together.
    Args:
    population: a population of graphs  derived from growthTicks representing the plants 
    derived from the initial seedPlant and subsequent growth of nodes.
    """

    edge_xy: list[tuple[tuple[float, float], tuple[float, float]]] = [
        ((u.position.x, v.position.x), (u.position.y, v.position.y)) for u, v in population.edges()]
    edge_species: list[int] = [
        u.tree_species_id for u, v in population.edges()]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("x")
    ax.set_ylabel("y")

    for node in sorted(population):
        if node.node_type != NodeType.ROOT:
            ax.scatter(node.position.x, node.position.y,
                       marker='s', s=7, ec="b")
        else:
            ax.scatter(node.position.x, node.position.y,
                       marker='*', s=40, ec="r")

    # Plot the edges
    for i in range(len(edge_xy)):
        ax.plot(edge_xy[i][0], edge_xy[i][1],
                color=LIST_OF_SPECIES1[edge_species[i]].COLOUR)

    ax.axis('equal')
    plt.tight_layout()
    plt.show()


def plot_random_nutrient_contours(grid_size=(20, 20), num_contours=5):
    """
    Generates a grid of random nutrient levels and plots the height contours.

    Parameters:
    - grid_size: tuple of two ints (nx, ny), the size of the grid
    - num_contours: int, the number of contour levels to plot
    """

    # Generate a grid of points
    x = np.linspace(0, 100, grid_size[0])
    y = np.linspace(0, 100, grid_size[1])
    X, Y = np.meshgrid(x, y)

    # Generate random heights at each grid point
    Z = np.random.rand(grid_size[0], grid_size[1])

    # Plot the contours
    plt.figure(figsize=(8, 6))
    contour = plt.contour(X, Y, Z, num_contours, cmap='gist_earth')
    plt.clabel(contour, inline=True, fontsize=8)
    plt.title("Nutrient Contours")
    plt.xlabel("X")
    plt.ylabel("Y")
    # plt.colorbar(contour)
    plt.show()



def GetBranchProbabilityFromCompetitor(node: PlantNode2D, searchgraph: nx.DiGraph, comp_interactions: defaultdict):
    '''This searches all trees for a node closer to  this node than the competition distance.

    Args    :   Node; the current node for which we are searching for competitors.
                searchgraph : nxDigraph ; the graph of all nodes to be searched for sompetitors
    Returns :   a tuple
            :   True, Boolean if a competitor node is found, false if not,  
            :   tree_species_id of the competitor node determining the reduction in probabilities, 
                or geometric properties of this node for future growth
    '''
    predecessors = list(searchgraph.predecessors(node))

    specie = LIST_OF_SPECIES1[node.tree_species_id]

    for otherPlantNode in nx.nodes(searchgraph):
        if otherPlantNode == node or predecessors.__contains__(otherPlantNode):
            continue
        if otherPlantNode.position.subtract(node.position).rho < LIST_OF_SPECIES1[node.tree_species_id].INTER_DISTANCE:
            print("Found a competitor node for " + str(node.tree_species_id),
                  " with species  : " + str(otherPlantNode.tree_species_id))
            # Increment comp_interactions count
            comp_interactions[node.tree_species_id][otherPlantNode.tree_species_id] += 1

            # if the nodes are of the same species probability is intraspecific, otherwise interspecific
            if node.tree_species_id == otherPlantNode.tree_species_id:
                branchprobability = specie.TERMINAL_GROWTH_PROB - specie.INTRA_COMP_PROB
            else:
                branchprobability = specie.TERMINAL_GROWTH_PROB - specie.INTER_COMP_PROB
         
            
            # in case the settings are incorrect and the branchprobability is negative. 
            if branchprobability < 0 : branchprobability = 0
           # return True, otherPlantNode.tree_species_id
            return True, branchprobability

    return False, specie.TERMINAL_GROWTH_PROB

def generate_no_grow_areas(num_areas: int, habitat_size: int, area_size: float):
    """Generate random square areas within the habitat that reduce branch probabilities."""
    no_grow_areas = []
    if NO_GROW_REGULAR:
        # Create a regular chessboard array of no-grow areas
        num_areas_per_side = habitat_size // area_size
        for i in range(num_areas_per_side):
            for j in range(num_areas_per_side):
                if (i + j) % 2 == 0:  # Create a chessboard pattern
                    x = i * area_size
                    y = j * area_size
                    no_grow_areas.append((x, y, area_size))
    else:
        attempts = 0
        max_attempts = num_areas * 10  # Limit the number of attempts to avoid infinite loops

        while len(no_grow_areas) < num_areas and attempts < max_attempts:
            x = random.uniform(0, habitat_size - area_size)
            y = random.uniform(0, habitat_size - area_size)
            new_area = (x, y, area_size)

            # Check for overlap with existing areas
            overlap = False
            for (existing_x, existing_y, existing_size) in no_grow_areas:
                if (x < existing_x + existing_size and x + area_size > existing_x and
                    y < existing_y + existing_size and y + area_size > existing_y):
                    overlap = True
                    break

            if not overlap:
                no_grow_areas.append(new_area)
            attempts += 1
        if attempts == max_attempts:
            print("Warning: Maximum attempts reached. Some no-grow areas may overlap.")
   
    return no_grow_areas

def check_no_grow_area(position, no_grow_areas):
    """Check if the position is within any no-grow area and return the reduction percentage."""
    for (x, y, size) in no_grow_areas:
        if x <= position.x <= x + size and y <= position.y <= y + size:
            return NO_GROW_REDUCTION_PERCENT # Reduction percentage
    return 0

def plot_no_grow_areas(no_grow_areas, habitat_size: int):
    """Plot the no-grow areas as grey squares within the habitat."""
    fig, ax = plt.subplots()
    ax.set_xlim(0, habitat_size)
    ax.set_ylim(0, habitat_size)
    ax.set_aspect('equal')

    for (x, y, size) in no_grow_areas:
        rect = plt.Rectangle((x, y), size, size, color='grey', alpha=0.5)
        ax.add_patch(rect)

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('No-Grow Areas within Habitat')
    plt.show()

def InDeadGround(position, no_grow_areas):
    """Check if the position is within any no-grow area and return True and probability=0 if it is."""
    for (x, y, size) in no_grow_areas:
        if x <= position.x <= x + size and y <= position.y <= y + size:
            print("Position", position, "is within a no-grow area.")
            return True, 0
    return False, None
   
# I will need to delete items  once death is implemented