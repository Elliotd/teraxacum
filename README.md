# Teraxacum

## Installation manual

- Install python 3.10.11
- Install VScode, install python extension, install autoDocstring for autogenerating comments
- Create python virtual environment `cmd+shif+P` write `Python : Create environment`
  - Make sure to select the requirements.txt as a dependency
- Run `main.py` program
