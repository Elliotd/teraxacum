import networkx as nx
from GrowthPatterns import GrowNode
from vector import VectorObject2D
from plantNode2D import PlantNode2D
from settings import *
from plantNode2D import NodeType
from util import *
from collections import defaultdict, Counter


class PlantManager:
    """This is the 2D version of the plant development algorithm
    """
    graph: nx.DiGraph = None
    """Graph containing all the plant nodes"""

    roots: list[PlantNode2D] = []
    """List of all roots of plants to enable iteration over growth cycle"""
    tree_id_counter: int = 0
    tree_id_size: list[float] = [] 
    tree_species_id_size : list[float] = []

    """total sizes of the species and the individual plants: It will iterate 
        over sp_pop_sizes[0,1,2]"""
    boundingBoxes: list[(VectorObject2D, VectorObject2D)] = []
    """List of bounding boxes linked to a specific root"""

    def __init__(self) -> None:
        # You are likely to use directed graphs and also growing network
        # https://en.wikipedia.org/wiki/Evolving_network
        # Perhaps one of the function might accelerate graph creation
        # gn_graph, gnc_graph, gnr_graph
        self.graph = nx.DiGraph()
        # Initialize the tree_id_size list
        self.tree_id_size: list[float] = [0.0] * sum(SP_POP_SIZES) 
        self.tree_species_id_size : list[float] = [0.0] * len(SP_POP_SIZES)
       # self.interactions = {}  # Dictionary to keep track of interactions
        self.comp_interactions = defaultdict(Counter)  # Initialize comp_interactions counter
        if HABITAT_NO_GROW:
            self.no_grow_areas = generate_no_grow_areas(NUM_NOGROW_AREAS, HABITAT_SIZE,NO_GROW_SIZE)
          # Generate random reduction areas
            if PLOT_HABITAT:
                plot_no_grow_areas(self.no_grow_areas, HABITAT_SIZE)
        else:
            self.no_grow_areas = []    # empty set of no no_grow_areas are defined    

    def seedPlant(self, position: VectorObject2D, direction: VectorObject2D, tree_species_id):
        """This function seeds a tree and saves the initial plant root, identifying it 
            as a particular species

        Args:
            position (VectorObject2D): Position of the initial tree
            direction (VectorObject2D): direction of growth of the initial tree
            tree_sp_id (SpeciesType) : identifying species specific parameters for growth 
        """

        plantNode: PlantNode2D = PlantNode2D(
            position, tree_species_id, direction)
        # set the first node_type to be SEED_NODE, to identify the origin of the plant
        plantNode.node_type = NodeType.ROOT
        plantNode.tree_species_id = tree_species_id
        #increment tree_id  and update tree_id_size 
        plantNode.tree_id = self.tree_id_counter
        #print ("seed plant 1: Tree ID ", plantNode.tree_id)      
        self.tree_id_counter += 1
        plantNode.int_node_distance = 0
        self.tree_species_id_size[plantNode.tree_species_id] = plantNode.int_node_distance
        self.roots.append(plantNode)
        self.graph.add_node(plantNode, position=[])
        self.boundingBoxes.append((position, position))
        # Initialize plant_size
        self.plant_size = [[0.0 for _ in range(PLANT_NUM)] for _ in range(SP_NUM)]
        # Dictionary to keep track of interactions 

    def processGrowthTick(self):
        """This function will process growth ticks for all the seeded plants 
        Args:
            position (VectorObject2D): Position of the node to be grown
        """

        newNodeList: list[PlantNode2D] = []
        newEdgesList: list[tuple[PlantNode2D, PlantNode2D]] = []
        comp_interactions = defaultdict(Counter)  # Initialize comp_interactions counter
        for i in range(len(self.roots)):
            # Do something with the nodes
            currentRoot = self.roots[i]
            # Query root species
            specie = LIST_OF_SPECIES1[currentRoot.tree_species_id]
            # append nodes if left, right, or reduced by competition dependent on species parameters
            descendents: list[PlantNode2D] = nx.descendants(
                self.graph, currentRoot)
            
            for node in descendents:
                # not sure why im using the graph here rather than the newNodelist but it works
                 # check if the plant is in a no grow area and kill node if so 
                in_dead_ground, _ = InDeadGround(node.position, self.no_grow_areas)
                if in_dead_ground:
                    print("In dead ground0",in_dead_ground)
                    node.node_type = NodeType.DEAD
                
                if len(list(self.graph.successors(node))) == 0:
                    #check bounding box
                    subgraph_nodes = []
                    for i, (min_corner, max_corner) in enumerate(self.boundingBoxes):
                        if min_corner.x <= node.position.x <= max_corner.x and min_corner.y <= node.position.y <= max_corner.y:
                            subgraph_nodes.extend(nx.descendants(self.graph, self.roots[i]))
                            subgraph_nodes.append(self.roots[i])

                    subgraph = self.graph.subgraph(subgraph_nodes)
                    # if no competitor plants are found grow the plant with no competition
                    if len(subgraph_nodes) == 0:
                        print ("No competitor plants found")                 
                        tempNodeList, tempEdgeList = GrowNode(node, self.graph, comp_interactions)
                        newNodeList = newNodeList + tempNodeList
                        newEdgesList = newEdgesList + tempEdgeList 
                        # Update tree_id_size for each added node with node.int_node_distance 
                        for tempEdge in tempEdgeList:
                            self.tree_id_size[node.tree_id] += tempEdge[0].int_node_distance
                            self.tree_species_id_size[node.tree_species_id] += tempEdge[0].int_node_distance
                    else: 
                        print ("competitor plants found")
                # Find neighbouring nodes and environmental levels to adk=just
                # growth parameters before insertion

                # Use the other root do a graph search using the graph library
                # Read docs for searches in graph
                # Use the bounding box to make the search faster

                # Modify object that defines growth thanks to the graph library

                # Use object that define growth to add nodes in the graph without forgetting adding the edges

                # Store data for the next growth cycle in the the nodes of the current tree

                # Keep track of the new nodes that you just grew and update the specific bounding box
            tree_id = currentRoot.tree_id
            
            if len(descendents) == 0:



                newNode = create_node_in_2Ddirection(currentRoot.position,
                                                     currentRoot.direction,
                                                     specie.LATERAL_ANGLE,
                                                     specie.LATERAL_SD,
                                                     True,
                                                     specie.MAIN_INTERNODE,
                                                     specie.MAIN_INTN_SD,
                                                     NodeType.UNBRANCHED,
                                                     currentRoot.tree_species_id,
                                                     tree_id,
                                                     self.no_grow_areas
                                                     ) 
                self.graph.add_node(newNode)
                # Don't forget to add the edges or the graphs won't make sense
                self.graph.add_edge(self.roots[i], newNode)
                continue

            # update the bounding boxes
            for j in range(len(self.roots)):
                if j == i:
                    continue
                otherRoot = self.roots[j]
                self.updateBoundingBox(j, otherRoot)

        # Add nodes at the end of the process
        self.graph.add_nodes_from(newNodeList)
        # Don't forget to add the edges or the graphs won't make sense
        self.graph.add_edges_from(newEdgesList)

        # Increment plant_size for each generation
        for node in self.graph.nodes:
            self.plant_size[node.tree_species_id][node.tree_id] += node.int_node_distance

        self.comp_interactions = comp_interactions

    def updateBoundingBox(self, plantID: int, node: PlantNode2D):
        """This function uses the plant node in order to update bounds of a plant

        Args:
            plantID (int): Position in the list of tracked roots
            node (PlantNode2D): Node that has been grown      
        """
        # for each plant create a bounding box

        currentBoundingBox = self.boundingBoxes[plantID]
        # Do tests with the position of the current node

        if node.position.x > currentBoundingBox[0].x:
            currentBoundingBox[0].x = node.position.x
        if node.position.x < currentBoundingBox[1].x:
            currentBoundingBox[1].x = node.position.x
        if node.position.y > currentBoundingBox[0].y:
            currentBoundingBox[0].y = node.position.y
        if node.position.y < currentBoundingBox[1].y:
            currentBoundingBox[1].y = node.position.y

        self.boundingBoxes[plantID] = currentBoundingBox
        # But the current bounding box has been updated
  
 
    def printPlantSizesData(self):
        """Prints the plant sizes for all tree_species_id and tree_id ordered by tree_species_id."""
        for species_id in range(len(self.plant_size)):
            print("Species ID ", species_id)
            for tree_id in range(len(self.plant_size[species_id])):
                if self.plant_size[species_id][tree_id] != 0:
                    print("Tree ID ",tree_id, self.plant_size[species_id][tree_id])
   

    def printCompInteractions(self):
        """Prints the competition interaction counts for all tree_species_id."""
        for species_id, competitors in self.comp_interactions.items():
            print("Species ID", species_id)
            for competitor_id, count in competitors.items():
                print(" Competitor Species ID ",competitor_id, count, "interactions")
