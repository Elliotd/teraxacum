from enum import Enum
import networkx as nx
from plantNode2D import PlantNode2D, NodeType
from settings import LIST_OF_SPECIES1, GrowthPattern
from util import create_node_in_2Ddirection, GetBranchProbabilityFromCompetitor
import numpy as np
from collections import defaultdict


def GrowNode(parentNode: PlantNode2D, graph: nx.DiGraph,comp_interactions: defaultdict):
    """Processes a growth tick to create children nodes from a parent node

    Args:
        parentNode (PlantNode2D): Parent node
        graph (nx.DiGraph): graph
        comp_interactions (defaultdict): Counter for the number of times each species finds a competitor

    Returns:
        _type_: Tuple of new nodes to add to the graph
    """
    newNodeList: list[PlantNode2D] = []
    newEdgesList: list[tuple[PlantNode2D, PlantNode2D]] = []

    match LIST_OF_SPECIES1[parentNode.tree_species_id].GROWTH_PATTERN:

        # match LIST_OF_SPECIES.GROWTH_PATTERN:
        case GrowthPattern.SYMPODIAL:
            SympodialGrowth(parentNode, graph, newNodeList, newEdgesList, comp_interactions)
        case GrowthPattern.MONOPODIAL:
            MonopodialGrowth(parentNode, graph, newNodeList, newEdgesList, comp_interactions)
        case GrowthPattern.ALTERNATE:
            AlternateGrowth(parentNode, graph, newNodeList, newEdgesList, comp_interactions)

    return newNodeList, newEdgesList


def SympodialGrowth(parentNode: PlantNode2D,
                    graph: nx.DiGraph,
                    newNodeList: list[PlantNode2D],
                    newEdgesList: list[tuple[PlantNode2D, PlantNode2D]],
                    comp_interactions: defaultdict):

    specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
    direction: bool  # growth direction left if true,right if false

    # Does this node has a competitor, found returning a tuple, booleaan and the competitor node species_id
    # note I may change the internode distance or the branch angle as well 
    branchprobability = GetBranchProbabilityFromCompetitor(parentNode, graph, comp_interactions)[1]

    match parentNode.node_type:
        case NodeType.UNBRANCHED | NodeType.LEFT_BRANCHED | NodeType.RIGHT_BRANCHED| NodeType.LR_BRANCHED:
            if np.random.uniform(0, 1) < branchprobability:
                #does it branch left, if so does it branch right as well
                direction = True
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.LEFT_BRANCHED, direction)
                if np.random.uniform(0, 1) < branchprobability:
                    #  does it branch right? if so parent becomes LR_BRANCHED
                    direction = False
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                     NodeType.LR_BRANCHED, direction) 
            else:
                direction = False # ignored
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                     NodeType.DEAD, direction)
                # It is dead. Implement a counter  


            


def AlternateGrowth(parentNode: PlantNode2D,
                    graph: nx.DiGraph,
                    newNodeList: list[PlantNode2D],
                    newEdgesList: list[tuple[PlantNode2D, PlantNode2D]],
                    comp_interactions: defaultdict):

    specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
    branchprobability = GetBranchProbabilityFromCompetitor(parentNode, graph, comp_interactions)[1]


    # search for competition with other nodes and growth parameters
    # from the environment reducing or enhancing growth probabilities

    match parentNode.node_type:
        case NodeType.UNBRANCHED:
            # is it starting  to grow left or right?
            if (branchprob := np.random.uniform(0, 1)) > 0.5:
                branchDirection = True
                if np.random.uniform(0, 1) < specie.TERMINAL_GROWTH_PROB:
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                          NodeType.MAIN_RIGHT,branchDirection)
                if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    branchDirection = False
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                          NodeType.LEFT_BRANCHED,branchDirection)
            else:
                branchDirection = False
                if np.random.uniform(0, 1) < specie.TERMINAL_GROWTH_PROB:
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                          NodeType.MAIN_LEFT, branchDirection)
                if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    branchDirection = True
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                          NodeType.RIGHT_BRANCHED,branchDirection)

        case NodeType.MAIN_RIGHT:
            branchDirection = False
            # insert a lateral on the right
            if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.UNBRANCHED,branchDirection)
            # insert a main left
            if np.random.uniform(0, 1) < specie.TERMINAL_GROWTH_PROB:
                branchDirection = True
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.MAIN_LEFT,branchDirection)
            else:
                parentNode.node_type = NodeType.DEAD

        case NodeType.MAIN_LEFT:
            # Lateral goes left, main goes right
            branchDirection = True
            if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.UNBRANCHED,branchDirection)
            if np.random.uniform(0, 1) < specie.TERMINAL_GROWTH_PROB:
                branchDirection = False
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.MAIN_RIGHT,branchDirection)
            else:
                parentNode.node_type = NodeType.DEAD


def MonopodialGrowth(parentNode: PlantNode2D,
                     graph: nx.DiGraph,
                     newNodeList: list[PlantNode2D],
                     newEdgesList: list[tuple[PlantNode2D, PlantNode2D]],
                     comp_interactions: defaultdict):

    specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
    branchprobability = GetBranchProbabilityFromCompetitor(parentNode, graph, comp_interactions)[1]
    # this needs to set left and right lateral branches,  and the main
    # setting  main as MAIN_L, MAIN_R or MAIN_LR or D

    match parentNode.node_type:
        case NodeType.UNBRANCHED:
            #using branchprobability rather than specie.MBRANCH_PROB
            if np.random.uniform(0, 1) < branchprobability:
                branchDirection = True
                ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                    NodeType.MAIN, branchDirection)
                
                if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    # add a left node - direction remains true
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                          NodeType.MAIN_L, branchDirection)
                    if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    # add a right node - direction = false
                        branchDirection = False
                        ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.MAIN_LR,branchDirection)
                else:
                    if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    # add a right node - direction = false
                        branchDirection = False
                        ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.MAIN_R,branchDirection)
            else :
                if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    branchDirection = True
                    # add a left node - direction remains true
                    ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                          NodeType.NO_MAIN_L, branchDirection)
                    if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    # add a right node - direction = false
                        branchDirection = False
                        ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.NO_MAIN_LR,branchDirection)
                else:
                    if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                    # add a right node - direction = false
                        branchDirection = False
                        ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.NO_MAIN_R,branchDirection)
                    else:
                        if np.random.uniform(0, 1) < specie.LATERAL_GROWTH_PROB:
                        # add a right node - direction = false
                            branchDirection = False
                            ProcessNodeTypeTransition(parentNode, newNodeList, newEdgesList,
                                                      NodeType.NO_MAIN_L,branchDirection)
                
                #parentNode.node_type = NodeType.DEAD

 
def ProcessNodeTypeTransition(parentNode: PlantNode2D,
                                          newNodeList: list[PlantNode2D],
                                          newEdgesList: list[tuple[PlantNode2D, PlantNode2D]],
                                          targetNodeType: NodeType, 
                                          branchDirection: bool):
    """_summary_
    Args:  node:  PlantNode2D  node object corresponding to plant section
                newNodeList:  list of current working nodes
                newEdgesList:  list of current working edges
                branchDirection: True if left, false if right
                nodetype : the  type of the current node
                branchtype: lateral or main, setting the  growth parameters  for the node.
    """

    if parentNode.tree_species_id == 0: # the branching pattern is sympodia
        match (parentNode.node_type, targetNodeType):

            case (parentNode.node_type.UNBRANCHED, targetNodeType.LEFT_BRANCHED) | (parentNode.node_type.UNBRANCHED, targetNodeType.RIGHT_BRANCHED) | (parentNode.node_type.UNBRANCHED, targetNodeType.LR_BRANCHED) | (parentNode.node_type.LEFT_BRANCHED, targetNodeType.LR_BRANCHED):

            #case (parentNode.node_type.UNBRANCHED, targetNodeType.LEFT_BRANCHED) | (parentNode.node_type.UNBRANCHED, targetNodeType.RIGHT_BRANCHED) | (parentNode.node_type.UNBRANCHED, targetNodeType.LR_BRANCHED)|(parentNode.node_type.LEFT_BRANCHED, targetNodeType.LR_BRANCHED)| (parentNode.node_type.LEFT_BRANCHED, targetNodeType.RIGHT_BRANCHED)|(parentNode.node_type.LR_BRANCHED, targetNodeType.RIGHT_BRANCHED)| (parentNode.node_type.LR_BRANCHED, targetNodeType.LEFT_BRANCHED)| (parentNode.node_type.LR_BRANCHED, targetNodeType.LR_BRANCHED):
                specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
                # for sympodial growth
                newNode = create_node_in_2Ddirection(parentNode.position,
                                             parentNode.direction,
                                             specie.TERMINAL_ANGLE,
                                             specie.TERMINAL_SD,
                                             branchDirection,  # This depends on the initial configuration
                                             specie.MAIN_INTERNODE,
                                             specie.MAIN_INTN_SD,
                                             targetNodeType,
                                             parentNode.tree_species_id,
                                             parentNode.tree_id,
                                             )
                newNode.node_type = NodeType.UNBRANCHED
                parentNode.node_type = targetNodeType
                newNodeList.append(newNode)
                newEdgesList.append((parentNode, newNode)) 
            
            case (parentNode.node_type.UNBRANCHED, targetNodeType.DEAD)| (parentNode.node_type.LEFT_BRANCHED, targetNodeType.DEAD)| (parentNode.node_type.RIGHT_BRANCHED, targetNodeType.DEAD)|(parentNode.node_type.DEAD, targetNodeType.DEAD)|(parentNode.node_type.DEAD, targetNodeType.RIGHT_BRANCHED): 
                parentNode.node_type = targetNodeType
            case _: 
                print("Warning, growth case %d, %d not implemented" %
                    (parentNode.node_type.value, targetNodeType.value))
        

    if parentNode.tree_species_id == 1: # the branching pattern is  alternate
         match (parentNode.node_type, targetNodeType):
            case (NodeType.MAIN, NodeType.MAIN) | (NodeType.UNBRANCHED, NodeType.MAIN_LEFT) | (NodeType.UNBRANCHED, NodeType.MAIN_RIGHT) | (NodeType.MAIN_LEFT, NodeType.MAIN_RIGHT)| (NodeType.MAIN_RIGHT, NodeType.MAIN_LEFT):
                specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
                # for alternate  growth    
                newNode = create_node_in_2Ddirection(parentNode.position,
                                             parentNode.direction,
                                             specie.TERMINAL_ANGLE,
                                             specie.TERMINAL_SD,
                                             branchDirection ,  # This depends on the initial configuration
                                             specie.MAIN_INTERNODE,
                                             specie.MAIN_INTN_SD,
                                             targetNodeType,
                                             parentNode.tree_species_id,
                                             parentNode.tree_id,
                                             )
                if branchDirection == True:
                    newNode.node_type = NodeType.MAIN_LEFT
                else: 
                    newNode.node_type = NodeType.MAIN_RIGHT

                parentNode.node_type = targetNodeType
                newNodeList.append(newNode)
                newEdgesList.append((parentNode, newNode))
            

            case(NodeType.MAIN_LEFT, NodeType.UNBRANCHED)|(NodeType.MAIN_RIGHT, NodeType.UNBRANCHED):
                specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
                # for alternate  growth
                newNode = create_node_in_2Ddirection(parentNode.position,
                                             parentNode.direction,
                                             specie.LATERAL_ANGLE,
                                             specie.LATERAL_SD,
                                             branchDirection,  # This depends on the initial configuration
                                             specie.LATERAL_INTERNODE,
                                             specie.LATERAL_INTERNODE_SD,
                                             targetNodeType,
                                             parentNode.tree_species_id,
                                             parentNode.tree_id,
                                             )
            
                newNode.node_type = targetNodeType
                newNode.node_type = NodeType.UNBRANCHED
                parentNode.node_type = targetNodeType
                newNodeList.append(newNode)
                newEdgesList.append((parentNode, newNode))
            case (NodeType.MAIN, NodeType.UNBRANCHED)| (NodeType.MAIN_LEFT, NodeType.RIGHT_BRANCHED)| (NodeType.MAIN_RIGHT, NodeType.LEFT_BRANCHED):

                specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
                # for alternate  growth
                newNode = create_node_in_2Ddirection(parentNode.position,
                                             parentNode.direction,
                                             specie.LATERAL_ANGLE,
                                             specie.LATERAL_SD,
                                             branchDirection,  # This depends on the initial configuration
                                             specie.LATERAL_INTERNODE,
                                             specie.LATERAL_INTERNODE_SD,
                                             targetNodeType,
                                             parentNode.tree_species_id,
                                             parentNode.tree_id,
                                             )
            
                newNode.node_type = targetNodeType
                newNode.node_type = NodeType.MAIN
                parentNode.node_type = targetNodeType
                newNodeList.append(newNode)
                newEdgesList.append((parentNode, newNode))

            case (NodeType.MAIN, NodeType.DEAD)| (NodeType.MAIN_LEFT, NodeType.DEAD)| (NodeType.MAIN_RIGHT, NodeType.DEAD)|(NodeType.UNBRANCHED, NodeType.DEAD)| (NodeType.NO_.MAIN_R, NodeType.DEAD)| (NodeType.NO_MAIN_L, NodeType.DEAD) :
                parentNode.node_type = targetNodeType
            case _ : 
                print("Warning, growth case %d, %d not implemented" %
                     (parentNode.node_type.value, targetNodeType.value))
    
    if parentNode.tree_species_id == 2: # the branching pattern is monopodial
        match (parentNode.node_type, targetNodeType):
            case(NodeType.UNBRANCHED, NodeType.MAIN) | (NodeType.UNBRANCHED, NodeType.NO_MAIN_LR)| (NodeType.UNBRANCHED, NodeType.NO_MAIN_R) | (NodeType.UNBRANCHED, NodeType.NO_MAIN_L)|(NodeType.MAIN, NodeType.MAIN)| (NodeType.MAIN_LR, NodeType.UNBRANCHED)| (NodeType.MAIN_LR, NodeType.MAIN_L):
                specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
                # for monopodial  growth
                newNode = create_node_in_2Ddirection(parentNode.position,
                                             parentNode.direction,
                                             specie.TERMINAL_ANGLE,
                                             specie.TERMINAL_SD,
                                             branchDirection,  # This depends on the initial configuration
                                             specie.MAIN_INTERNODE,
                                             specie.MAIN_INTN_SD,
                                             targetNodeType,
                                             parentNode.tree_species_id,
                                             parentNode.tree_id,
                                             )
                newNode.node_type = targetNodeType
                newNode.node_type = NodeType.UNBRANCHED
                parentNode.node_type = targetNodeType
                newNodeList.append(newNode)
                newEdgesList.append((parentNode, newNode))               

           
            case(NodeType.MAIN, NodeType.MAIN_L)| (NodeType.MAIN, NodeType.MAIN_R)| (NodeType.MAIN_RIGHT, NodeType.LEFT_BRANCHED)|(NodeType.MAIN_L, NodeType.NO_MAIN_LR)| (NodeType.MAIN_L, NodeType.MAIN_LR) | (NodeType.MAIN_LR, NodeType.NO_MAIN_L)| (NodeType.MAIN_LR, NodeType.MAIN_RIGHT)| (NodeType.MAIN_LR, NodeType.MAIN_L)| (NodeType.NO_MAIN_L, NodeType.NO_MAIN_LR):
                specie = LIST_OF_SPECIES1[parentNode.tree_species_id]
                # for monopodial  growth
               
                newNode = create_node_in_2Ddirection(parentNode.position,
                                             parentNode.direction,
                                             specie.LATERAL_ANGLE,
                                             specie.LATERAL_SD,
                                             branchDirection,  # This depends on the initial configuration
                                             specie.LATERAL_INTERNODE,
                                             specie.LATERAL_SD,
                                             targetNodeType,
                                             parentNode.tree_species_id,
                                             parentNode.tree_id,
                                             )
            
                newNode.node_type = targetNodeType
                newNode.node_type = NodeType.UNBRANCHED
                parentNode.node_type = targetNodeType
                newNodeList.append(newNode)
                newEdgesList.append((parentNode, newNode))

            case(NodeType.MAIN, NodeType.DEAD)|( NodeType.MAIN_L, NodeType.DEAD)| ( NodeType.MAIN_R, NodeType.DEAD)| ( NodeType.LEFT_BRANCHED, NodeType.DEAD) | ( NodeType.NO_MAIN_LR, NodeType.DEAD)| ( NodeType.MAIN_LR, NodeType.DEAD) | ( NodeType.NO_MAIN_L, NodeType.DEAD)| (NodeType.MAIN_RIGHT, NodeType.DEAD)| ( NodeType.MAIN_L, NodeType.DEAD)| (NodeType.NO_MAIN_LR, NodeType.DEAD):
                parentNode.node_type = targetNodeType

            case _:
                 print("Warning, growth case %d, %d not implemented" %                   
                    (parentNode.node_type.value, targetNodeType.value))