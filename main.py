import random
from plantManager import PlantManager
from vector import VectorObject2D
from settings import SP_POP_SIZES, LIST_OF_SPECIES1, GENERATIONS, HABITAT_SIZE
from util import displayGraph, plot_random_nutrient_contours
from collections import defaultdict

plantManager = PlantManager()

# SETUP

# Set the environment and  plot_random_nutrient_contours()

# Adding plants to the simulation for each species
for species_id in range(len(LIST_OF_SPECIES1)):
    for species_pop_size in range(SP_POP_SIZES[species_id]):
        position: VectorObject2D = VectorObject2D(x=random.uniform(
            1, HABITAT_SIZE), y=random.uniform(1, HABITAT_SIZE))
        direction: VectorObject2D = position.rotateZ(random.uniform(0, 6.283))
        # add numbers of plants of each species given by SP_POP_SIZES[i]
        plantManager.seedPlant(position, direction, species_id)

# RUN
# Make GENERATIONS growth ticks go
for i in range(GENERATIONS):
    # This is a growth tick to see if the plant grows
    plantManager.processGrowthTick()
    displayGraph(plantManager.graph)

# RESULT
# Show the plant on a graph.
# displayGraph(plantManager.graph)
# Collect data
plantManager.printPlantSizesData()
# Print interactions
plantManager.printCompInteractions()
