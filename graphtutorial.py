import networkx as nx
import matplotlib.pyplot as plt

G = nx.Graph()
# You can add one node at a time,
G.add_node(1)
# You can also add nodes along with node attributes if your container yields 2-tuples 
# of the form (node, node_attribute_dict):
G.add_nodes_from([

    (4, {"color": "red"}),

    (5, {"color": "green"}),

])

# Nodes from one graph can be incorporated into another:
H = nx.path_graph(10)


# you could use the graph H as a node in G
G.add_nodes_from(H)

# G can also be grown by adding one edge at a time,

G.add_edge(1, 2)

# by adding a list of edges,
e = (2, 3)

# or by adding any ebunch of edges. An ebunch is any iterable container of edge-tuples. 
# An edge-tuple can be a 2-tuple of nodes or a 3-tuple with 2 nodes followed by an edge 
# attribute dictionary, e.g., (2, 3, {'weight': 3.1415}). 
# Edge attributes are discussed further below.
G.add_edge(*e)  # unpack edge tuple*

G.add_edges_from([(1, 2), (1, 3)])

G.add_edges_from([(1, 2), (1, 3)])
nx.draw(G) 
plt.show()
G.add_node(1)

G.add_edge(1, 2)

G.add_node("spam")        # adds node "spam"

G.add_nodes_from("spam")  # adds 4 nodes: 's', 'p', 'a', 'm'

G.add_edge(3, 'm')

# At this stage the graph G consists of 8 nodes and 3 edges, as can be seen by:
G.number_of_nodes()
G.number_of_edges()


# The order of adjacency reporting (e.g., G.adj, G.successors, G.predecessors) is the
# order of edge addition. However, the order of G.edges is the order of the adjacencies
# which includes both the order of the nodes and each node’s adjacencies. 
# See example below:
DG = nx.DiGraph()

DG.add_edge(2, 1)   # adds the nodes in order 2, 1

DG.add_edge(1, 3)

DG.add_edge(2, 4)

DG.add_edge(1, 2)

assert list(DG.successors(2)) == [1, 4]

assert list(DG.edges) == [(2, 1), (2, 4), (1, 3), (1, 2)]


# We can examine the nodes and edges.
list(G.nodes)
list(G.edges)

list(G.adj[1])  # or list(G.neighbors(1))

G.degree[1]  # the number of edges incident to 1

# One can specify to report the edges and degree from a subset of all nodes using an nbunch.
# An nbunch is any of: None (meaning all nodes), a node, or an iterable container of 
# nodes that is not itself a node in the graph.
G.edges([2, 'm'])

G.degree([2, 3])


# One can remove nodes and edges from the graph in a similar fashion to adding. 
# Use methods Graph.remove_node(), Graph.remove_nodes_from(), Graph.remove_edge() 
# and Graph.remove_edges_from(), e.g.
G.remove_node(2)

G.remove_nodes_from("spam")

list(G.nodes)
G.remove_edge
nx.draw(DG) 
plt.show()